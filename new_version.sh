#!/bin/bash

link=$1
port=$2

version=$(cat ./src/main/resources/version.txt)

rev_version=$(echo "$version"|rev)
version_base=$(echo "${rev_version#*.}"|rev)
version_number=$(echo "$rev_version"|cut -d'.' -f1|rev)

new_version="$version_base.$((version_number+=1))"
echo "$new_version" > ./src/main/resources/version.txt
echo "new version will be $new_version"

if [ "$link" != "" ]; then
  if [ "$port" != "" ]; then
    echo "sending jar to $link on port $port"
    scp -P "$port" "./build/libs/mandelbrot.jar" "$link"
  else
    echo "sending jar to $link on default ssh port (port 22)"
    scp "./build/libs/mandelbrot.jar" "$link"
  fi
fi
