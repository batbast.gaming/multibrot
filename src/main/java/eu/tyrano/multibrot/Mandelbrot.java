package eu.tyrano.multibrot;

import eu.tyrano.multibrot.maths.Complex;

public class Mandelbrot {

    private final double power;
    private final int maxIteration;
    private final int width;
    private final int height;
    private final double zoom;
    private final double x;
    private final double y;

    public Mandelbrot(double power, int maxIteration, int width, int height, double zoom, double x, double y) {
        this.power = power;
        this.maxIteration = maxIteration;
        this.width = width;
        this.height = height;
        this.zoom = zoom;
        if (x == 0 && y == 0) {
            this.x = -2.5 / 2d;
            this.y = 1.25 / 2d;
        } else {
            this.x = x;
            this.y = y;
        }
    }

    public String calculate() {
        Complex z;
        Complex c;
        int count;
        StringBuilder data = new StringBuilder();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                z = new Complex(0, 0);
                c = new Complex((3.5 * (x - (width / 2d) * zoom) * zoom) / width + (this.x),
                        (-3.5 * (y - (height / 2d) * zoom) * zoom) / height - (this.y));

                count = 0;
                while (count < maxIteration && z.absolute() < 2) {
                    count++;
                    z.power(this.power);
                    z.add(c);
                }
                data.append(((double) count / (double) maxIteration) * 360).append(",");
            }
        }
        return data.deleteCharAt(data.length() - 1).toString();
    }
}
