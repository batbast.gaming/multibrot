package eu.tyrano.multibrot;

import eu.tyrano.multibrot.maths.Quaternion;

public class MandelBulb {

    private final int width;
    private final int height;
    private final int depth;
    private final int frame;
    private final int totalFrames;
    private final int maxIteration;
    private final double factorX;
    private final double factorY;

    public MandelBulb(int width, int height, int depth, int frame, int totalFrame, int maxIteration) {
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.frame = frame;
        this.totalFrames = totalFrame;
        this.maxIteration = maxIteration;

        this.factorX = -2.5 / 2;
        this.factorY = 1.25 / 2d;
    }

    public float calculate(int x, int y, int z) {
        Quaternion zQuaternion = new Quaternion(0, 0, 0, 0);
        Quaternion cQuaternion = new Quaternion(
                (3.5 * (x - (width / 2d))) / width + (this.factorX),
                (-3.5 * (y - (height / 2d))) / height - (this.factorY),
                (double) z / (depth / 4d),
                (double) frame / (totalFrames / 4d));
        int count = 0;
        while (count < maxIteration && zQuaternion.absolute() < 2) {
            count++;
            zQuaternion.multiply(zQuaternion);
            zQuaternion.add(cQuaternion);
        }
        return (float) count;
    }
}