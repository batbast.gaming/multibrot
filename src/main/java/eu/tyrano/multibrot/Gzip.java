package eu.tyrano.multibrot;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Gzip {

    public void zipString(String data, File target) {
        try (GZIPOutputStream gos = new GZIPOutputStream(new FileOutputStream(target))) {
            gos.write(data.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String unzipString(File source) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try (GZIPInputStream gis = new GZIPInputStream(new FileInputStream(source))) {
            byte[] buffer = new byte[1024];
            int len;
            while ((len = gis.read(buffer)) > 0) {
                output.write(buffer, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return output.toString();
    }
}
