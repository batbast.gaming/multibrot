package eu.tyrano.multibrot.maths;

import static java.lang.Math.*;

public class Complex {
    private double real;
    private double imaginary;

    public Complex(double real, double imaginary) {
        this.real = real;
        this.imaginary = imaginary;
    }

    public Complex(Complex C) {
        this.real = C.real;
        this.imaginary = C.imaginary;
    }

    public double getReal() {
        return real;
    }

    public double getImaginary() {
        return imaginary;
    }

    public void multiply(Complex M) {
        multiply(M.real, M.imaginary);
    }

    public void multiply(double realM, double imaginaryM) {
        double newReal = (this.real * realM) - (this.imaginary * imaginaryM);
        this.imaginary = (this.real * imaginaryM) + (realM * this.imaginary);
        this.real = newReal;
    }

    public void power(double power) {
        double rPowered = pow(absolute(), power);
        double theta = atan2(this.imaginary, this.real);
        double powTheta = power * theta;
        this.real = cos(powTheta);
        this.imaginary = sin(powTheta);
        multiply(rPowered, 0);
    }

    public double absolute() {
        return sqrt(pow(this.real, 2) + pow(this.imaginary, 2));
    }

    public void add(Complex A) {
        add(A.real, A.imaginary);
    }

    public void add(double readA, double imaginaryA) {
        this.real += readA;
        this.imaginary += imaginaryA;
    }

    @Override
    public String toString() {
        return "Complex{" +
                "real=" + real +
                ", imaginary=" + imaginary +
                '}';
    }
}