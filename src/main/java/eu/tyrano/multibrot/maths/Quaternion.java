package eu.tyrano.multibrot.maths;

import static java.lang.Math.*;

public class Quaternion {
    private double real;
    private double i;
    private double j;
    private double k;

    public Quaternion(double real, double i, double j, double k) {
        this.real = real;
        this.i = i;
        this.j = j;
        this.k = k;
    }

    public Quaternion(Quaternion Q) {
        this.real = Q.real;
        this.i = Q.i;
        this.j = Q.j;
        this.k = Q.k;
    }

    public double getReal() {
        return real;
    }

    public double getI() {
        return i;
    }

    public double getJ() {
        return j;
    }

    public double getK() {
        return k;
    }

    public void multiply(Quaternion Q) {
        multiply(Q.real, Q.i, Q.j, Q.k);
    }

    public void multiply(double real, double i, double j, double k) {
        // t0=(r0q0−r1q1−r2q2−r3q3)
        // t1=(r0q1+r1q0−r2q3+r3q2)
        // t2=(r0q2+r1q3+r2q0−r3q1)
        // t3=(r0q3−r1q2+r2q1+r3q0)
        double tmpReal = this.real;
        double tmpI = this.i;
        double tmpJ = this.j;
        double tmpK = this.k;

        this.real = ((tmpReal * real) - (tmpI * i) - (tmpJ * j) - (tmpK * k));
        this.i = ((tmpReal * i) + (tmpI * real) - (tmpJ * k) + (tmpK * j));
        this.j = ((tmpReal * j) + (tmpI * k) + (tmpJ * real) - (tmpK * i));
        this.k = ((tmpReal * k) - (tmpI * j) + (tmpJ * i) + (tmpK * real));
    }

    public void add(Quaternion Q) {
        add(Q.real, Q.i, Q.j, Q.k);
    }

    public void add(double real, double i, double j, double k) {
        this.real += real;
        this.i += i;
        this.j += j;
        this.k += k;
    }

    public double absolute() {
        return sqrt(pow(this.real, 2) + pow(this.i, 2) + pow(this.j, 2) + pow(this.k, 2));
    }
}