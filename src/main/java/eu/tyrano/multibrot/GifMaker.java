package eu.tyrano.multibrot;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class GifMaker {

    public GifMaker(String[] args) {
        File directory = new File(args[1]);
        int delay = Integer.parseInt(args[2]);

        File[] files = directory.listFiles();

        if (files != null && files.length != 0) {
            System.out.println(files.length + " to convert");
            try {
                BufferedImage first = ImageIO.read(files[0]);
                ImageOutputStream output = new FileImageOutputStream(new File("output.gif"));

                Giffer writer = new Giffer(output, first.getType(), delay, true);
                writer.writeToSequence(first);

                for (int i = 1; i < files.length; i++) {
                    BufferedImage next = ImageIO.read(files[i]);
                    writer.writeToSequence(next);
                    System.out.println("i = " + i);
                }

                writer.close();
                output.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }
}
