package eu.tyrano.multibrot;

import java.io.File;

public class CalculateSpecificZoomMandelbrot {

    private final int zoom;
    private final double power;
    private final int width;
    private final int height;
    private final double x;
    private final double y;
    private final Gzip gzip;

    public CalculateSpecificZoomMandelbrot(String[] args) {
        zoom = Integer.parseInt(args[1]);
        power = Double.parseDouble(args[2]);
        width = Integer.parseInt(args[3]);
        height = Integer.parseInt(args[4]);
        x = Double.parseDouble(args[5]);
        y = Double.parseDouble(args[6]);

        gzip = new Gzip();

        this.calcValues();
    }

    private void calcValues() {
        String mandelbrotData = new Mandelbrot(
                power, zoom * 255, width, height, 1d / zoom, x, y).calculate();

        File file = new File("mandelbrot_" + String.format("%06d", 0) + ".csv.gzip");

        gzip.zipString(mandelbrotData, file);
    }
}
