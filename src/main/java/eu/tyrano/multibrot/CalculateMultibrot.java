package eu.tyrano.multibrot;

import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;

public class CalculateMultibrot {

    private final AtomicInteger increment;
    private final int maxPower;
    private final double powerStart;
    private final double powerEnd;
    private final double powerDiff;
    private final int width;
    private final int height;
    private final Gzip gzip;

    public CalculateMultibrot(String[] args) {
        maxPower = Integer.parseInt(args[1]);
        powerStart = Double.parseDouble(args[2]);
        powerEnd = Double.parseDouble(args[3]);
        powerDiff = powerEnd - powerStart;
        width = Integer.parseInt(args[4]);
        height = Integer.parseInt(args[5]);
        increment = new AtomicInteger(-1);
        gzip = new Gzip();

        int coreCount = Runtime.getRuntime().availableProcessors() - 1;
        for (int j = 0; j < coreCount; j++) {
            new Thread(this::calcValues).start();
        }
    }

    private void calcValues() {
        while (powerStart + (increment.doubleValue() / maxPower) * powerDiff < powerEnd) {
            int i = increment.addAndGet(1);

            String mandelbrotData = new Mandelbrot(
                    (powerStart + ((double) i / maxPower) * powerDiff),
                    255, width, height, 1, 0, 0)
                    .calculate();

            File file = new File("mandelbrot_" + String.format("%06d", i) + ".csv.gzip");

            gzip.zipString(mandelbrotData, file);

            if (increment.intValue() % (maxPower / 100d) == 0) {
                System.out.println(increment.intValue() / (maxPower / 100d) + "%");
            }
        }
        increment.addAndGet(1);
    }
}
