package eu.tyrano.multibrot;

import Jama.Matrix;
import eu.tyrano.multibrot.printer.CubePosition;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class CalculateMandelbulb {
    private final int width;
    private final int height;
    private final int depth;
    private final int frameCount;
    private final int precision;

    private final int scaleFactor;

    private final AtomicInteger frameIncrement;

    private Matrix camPosition;

    private Matrix factorMatrix;

    private final int finalCamX;
    private final int finalCamY;
    private final int finalCamZ;

    private final double finalDirectionX;
    private final double finalDirectionY;
    private final double finalDirectionZ;

    private final double directionX;
    private final double directionY;
    private final double directionZ;

    private double screenRelativeX;
    private double screenRelativeY;
    private double screenRelativeZ;

    public CalculateMandelbulb(String[] args) {
        this.width = Integer.parseInt(args[1]);
        this.height = Integer.parseInt(args[2]);
        this.depth = Integer.parseInt(args[3]);
        this.frameCount = Integer.parseInt(args[4]);
        this.precision = Integer.parseInt(args[5]);
        this.scaleFactor = Integer.parseInt(args[6]);

        this.frameIncrement = new AtomicInteger(-1);

        File configFile = new File("config.txt");

        HashMap<String, Double> configValues = new HashMap<>();

        try {
            if (configFile.createNewFile()) {
                System.out.println("No config file found, creating one");

                FileWriter writer = new FileWriter(configFile);
                writer.write("""
                        #Configs for --draw3d operations
                        camera_x=15
                        camera_y=15
                        camera_z=-10
                        camera_x_final=15
                        camera_y_final=15
                        camera_z_final=-10
                        camera_rotation_x=0
                        camera_rotation_y=0
                        camera_rotation_z=0
                        camera_rotation_x_final=0
                        camera_rotation_y_final=0
                        camera_rotation_z_final=0
                        """);
                writer.close();

                System.out.println("File created: " + configFile.getName());
            }

            List<String> values = Files.readAllLines(configFile.toPath(), Charset.defaultCharset());

            for (String value : values) {
                if (!value.split("")[0].equals("#")) {
                    String[] nameAndValue = value.split("=");
                    configValues.put(nameAndValue[0], Double.parseDouble(nameAndValue[1]));
                }
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

        this.camPosition = new Matrix(new double[][]{
                {configValues.get("camera_x")},
                {configValues.get("camera_y")},
                {configValues.get("camera_z")}
        });

        this.finalCamX = configValues.get("camera_x_final").intValue();
        this.finalCamY = configValues.get("camera_y_final").intValue();
        this.finalCamZ = configValues.get("camera_z_final").intValue();

        this.finalDirectionX = configValues.get("camera_rotation_x_final");
        this.finalDirectionY = configValues.get("camera_rotation_x_final");
        this.finalDirectionZ = configValues.get("camera_rotation_x_final");

        this.directionX = configValues.get("camera_rotation_x");
        this.directionY = configValues.get("camera_rotation_y");
        this.directionZ = configValues.get("camera_rotation_z");
    }

    private double approximateCos(double a) {
        return Math.round(Math.cos(a) * 1000d) / 1000d;
    }

    private double approximateSin(double a) {
        return Math.round(Math.sin(a) * 1000d) / 1000d;
    }

    public void computeFrame() {
        while (this.frameIncrement.intValue() < this.frameCount - 1) {
            int currentFrame = this.frameIncrement.addAndGet(1);

            makeProgress(depth, currentFrame);

            BufferedImage bufferedImage = new BufferedImage(
                    width * this.scaleFactor,
                    height * this.scaleFactor,
                    BufferedImage.TYPE_INT_RGB);

            Graphics2D graphics2D = bufferedImage.createGraphics();

            graphics2D.setColor(Color.WHITE);
            graphics2D.fillRect(0, 0, bufferedImage.getWidth(), bufferedImage.getHeight());

            MandelBulb mandelBulb = new MandelBulb(width, height, depth, currentFrame, frameCount, precision);

            short xCentre = (short) camPosition.get(0, 0);
            short yCentre = (short) camPosition.get(1, 0);
            short zCentre = (short) camPosition.get(2, 0);

            float distance = this.getMaxDistance(xCentre, yCentre, zCentre);

            float decrement = 0.8f;

            while (distance >= 0) {

                ArrayList<Cube3d> cubes = computeSphere(distance, decrement, mandelBulb, xCentre, yCentre, zCentre);

                cubes.sort(Comparator.comparing((Cube3d cube) -> cube.distance).reversed());

                for (Cube3d cube3d : cubes) {
                    printCube(graphics2D, cube3d, distance);
                }

                distance -= decrement;
            }

            File image = new File("mandelbulb_" + String.format("%06d", currentFrame) + ".png");

            try {
                ImageIO.write(bufferedImage, "PNG", image);
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("Image " + currentFrame + " successfully drawn");
        }
    }

    public ArrayList<Cube3d> computeSphere(float radius, float increment, MandelBulb mandelBulb, int xCentre, int yCentre, int zCentre) {

        ArrayList<Cube3d> cubes = new ArrayList<>();

        double angle = Math.atan2(radius, 0) - Math.atan2(radius, 1);

        double circleQuarter = Math.PI / 2;

        float radiusIncrement = 0;

        while (radiusIncrement < radius) {
            //gets distance from centre of circle by its diameter (radius * 2)
            short distanceFromCentre = (short) (Math.sqrt((radius * radius) - (radiusIncrement * radiusIncrement)));

            double currentAngle = 0;
            //increment around the circle
            while (currentAngle < circleQuarter) {
                short posX = (short) ((radiusIncrement * Math.cos(currentAngle)));
                short posY = (short) ((radiusIncrement * Math.sin(currentAngle)));
                CubePosition cubePosition = new CubePosition(posX, posY, distanceFromCentre);

                short[][] coordinates = new short[][]{
                        {(short) (xCentre + cubePosition.x()), (short) (yCentre + cubePosition.y()), (short) (zCentre + cubePosition.z())},
                        {(short) (xCentre + cubePosition.x()), (short) (yCentre + cubePosition.y()), (short) (zCentre - cubePosition.z())},
                        {(short) (xCentre + cubePosition.x()), (short) (yCentre - cubePosition.y()), (short) (zCentre + cubePosition.z())},
                        {(short) (xCentre + cubePosition.x()), (short) (yCentre - cubePosition.y()), (short) (zCentre - cubePosition.z())},
                        {(short) (xCentre - cubePosition.x()), (short) (yCentre + cubePosition.y()), (short) (zCentre + cubePosition.z())},
                        {(short) (xCentre - cubePosition.x()), (short) (yCentre + cubePosition.y()), (short) (zCentre - cubePosition.z())},
                        {(short) (xCentre - cubePosition.x()), (short) (yCentre - cubePosition.y()), (short) (zCentre + cubePosition.z())},
                        {(short) (xCentre - cubePosition.x()), (short) (yCentre - cubePosition.y()), (short) (zCentre - cubePosition.z())},
                };

                for (short[] position : coordinates) {
                    if (position[0] >= 0 && position[0] < width
                            && position[1] >= 0 && position[1] < height
                            && position[2] >= 0 && position[2] < depth) {

                        float color = mandelBulb.calculate(position[0], position[1], position[2] - (depth / 2)) / precision * 360;
                        if (color > 25) {
                            cubes.add(Cube3d.build(position[0], position[1], position[2], camPosition, color));
                        }
                    }
                }

                currentAngle += angle;
            }
            radiusIncrement += increment;
        }

        return cubes;
    }

    private short getMaxDistance(double camX, double camY, double camZ) {
        ArrayList<CubePosition> cubePositions = new ArrayList<>();
        cubePositions.add(new CubePosition((short) 0, (short) height, (short) 0));
        cubePositions.add(new CubePosition((short) 0, (short) height, (short) depth));
        cubePositions.add(new CubePosition((short) 0, (short) 0, (short) 0));
        cubePositions.add(new CubePosition((short) 0, (short) 0, (short) depth));
        cubePositions.add(new CubePosition((short) width, (short) height, (short) 0));
        cubePositions.add(new CubePosition((short) width, (short) height, (short) depth));
        cubePositions.add(new CubePosition((short) width, (short) 0, (short) 0));
        cubePositions.add(new CubePosition((short) width, (short) 0, (short) depth));

        double max = 0;
        for (CubePosition cubePosition : cubePositions) {
            max = Math.max(max, distance(cubePosition.x(), cubePosition.y(), cubePosition.z(), camX, camY, camZ));
        }
        return (short) Math.ceil(max);
    }

    private double distance(double x1, double y1, double z1, double x2, double y2, double z2) {
        return Math.sqrt(Math.pow(x1 - x2, 2) +
                Math.pow(y1 - y2, 2) +
                Math.pow(z1 - z2, 2));
    }

    public void compute() {
        int threadCount = Runtime.getRuntime().availableProcessors() - 1;
        for (int j = 0; j < threadCount && j < frameCount; j++) {
            new Thread(this::computeFrame).start();
        }
    }

    private void makeProgress(int totalFile, double i) {
        double progress = i / totalFile;

        double camPositionX = this.camPosition.get(0, 0);
        double camPositionY = this.camPosition.get(1, 0);
        double camPositionZ = this.camPosition.get(2, 0);

        this.camPosition = new Matrix(new double[][]{
                {camPositionX + ((finalCamX - camPositionX) * progress)},
                {camPositionY + ((finalCamY - camPositionY) * progress)},
                {camPositionZ + ((finalCamZ - camPositionZ) * progress)},
        });

        this.screenRelativeX = this.camPosition.get(0, 0);
        this.screenRelativeY = this.camPosition.get(1, 0);
        this.screenRelativeZ = this.camPosition.get(2, 0) * 3 / 4d;

        double camRotationX = directionX + ((finalDirectionX - directionX) * progress);
        double camRotationY = directionY + ((finalDirectionY - directionY) * progress);
        double camRotationZ = directionZ + ((finalDirectionZ - directionZ) * progress);

        Matrix matrixA = new Matrix(new double[][]{
                {1, 0, 0},
                {0, approximateCos(camRotationX), approximateSin(camRotationX)},
                {0, -approximateSin(camRotationX), approximateCos(camRotationX)}
        });

        Matrix matrixB = new Matrix(new double[][]{
                {approximateCos(camRotationY), 0, -approximateSin(camRotationY)},
                {0, 1, 0},
                {approximateSin(camRotationY), 0, approximateCos(camRotationY)}
        });

        Matrix matrixC = new Matrix(new double[][]{
                {approximateCos(camRotationZ), approximateSin(camRotationZ), 0},
                {-approximateSin(camRotationZ), approximateCos(camRotationZ), 0},
                {0, 0, 1}
        });

        this.factorMatrix = matrixA.times(matrixB).times(matrixC);
    }

    public record Cube3d(short x, short y, short z, double distance, float color) {
        static Cube3d build(int x, int y, int z, Matrix camPosition, float color) {
            return new Cube3d((short) x, (short) y, (short) z,
                    distanceToCamera(new int[]{x, y, z}, new int[]{x + 1, y + 1, z + 1}, camPosition),
                    color);
        }
    }

    private int[][] calcFace(int[] pointA, int[] pointB, int[] pointC, int[] pointD) {
        return new int[][]{
                {pointA[0], pointB[0], pointC[0], pointD[0]},
                {pointA[1], pointB[1], pointC[1], pointD[1]}
        };
    }

    private static double distanceToCamera(int[] pointA, int[] pointB, Matrix camPosition) {
        double centerX = ((double) pointA[0] + pointB[0]) / 2;
        double centerY = ((double) pointA[1] + pointB[1]) / 2;
        double centerZ = ((double) pointA[2] + pointB[2]) / 2;

        return Math.sqrt(Math.pow(centerX - camPosition.get(0, 0), 2)
                + Math.pow(centerY - camPosition.get(1, 0), 2)
                + Math.pow(centerZ - camPosition.get(2, 0), 2)
        );
    }

    private void printCube(Graphics2D graphics2D, Cube3d cube3d, float maxDistance) {

        int x = cube3d.x;
        int y = cube3d.y;
        int z = cube3d.z;

        int[][] points = {
                {x, y + 1, z},
                {x + 1, y + 1, z},
                {x + 1, y, z},
                {x, y, z},
                {x, y + 1, z + 1},
                {x + 1, y + 1, z + 1},
                {x + 1, y, z + 1},
                {x, y, z + 1}};

        int[] point3dA = points[0];
        int[] point3dB = points[1];
        int[] point3dC = points[2];
        int[] point3dD = points[3];
        int[] point3dE = points[4];
        int[] point3dF = points[5];
        int[] point3dG = points[6];
        int[] point3dH = points[7];

        int[] pointA = calcPoint(point3dA);
        int[] pointB = calcPoint(point3dB);
        int[] pointC = calcPoint(point3dC);
        int[] pointD = calcPoint(point3dD);
        int[] pointE = calcPoint(point3dE);
        int[] pointF = calcPoint(point3dF);
        int[] pointG = calcPoint(point3dG);
        int[] pointH = calcPoint(point3dH);

        int[][] square1 = calcFace(pointA, pointB, pointC, pointD);
        int[][] square2 = calcFace(pointE, pointF, pointG, pointH);
        int[][] square3 = calcFace(pointA, pointE, pointH, pointD);
        int[][] square4 = calcFace(pointB, pointF, pointG, pointC);
        int[][] square5 = calcFace(pointH, pointG, pointC, pointD);
        int[][] square6 = calcFace(pointE, pointF, pointB, pointA);

        ArrayList<CubeFace> faceCenters = new ArrayList<>(6);

        faceCenters.add(new CubeFace(square1, distanceToCamera(point3dA, point3dC, camPosition)));
        faceCenters.add(new CubeFace(square2, distanceToCamera(point3dE, point3dG, camPosition)));
        faceCenters.add(new CubeFace(square3, distanceToCamera(point3dA, point3dH, camPosition)));
        faceCenters.add(new CubeFace(square4, distanceToCamera(point3dB, point3dG, camPosition)));
        faceCenters.add(new CubeFace(square5, distanceToCamera(point3dH, point3dC, camPosition)));
        faceCenters.add(new CubeFace(square6, distanceToCamera(point3dE, point3dA, camPosition)));

        faceCenters.sort(Comparator.comparing((CubeFace face) -> face.distance));

        int[][] face1 = faceCenters.get(0).value;
        int[][] face2 = faceCenters.get(1).value;
        int[][] face3 = faceCenters.get(2).value;

        Polygon polygonFace1 = new Polygon(face1[0], face1[1], 4);
        Polygon polygonFace2 = new Polygon(face2[0], face2[1], 4);
        Polygon polygonFace3 = new Polygon(face3[0], face3[1], 4);

        Color color = Color.getHSBColor(cube3d.color, (float) (maxDistance / cube3d.distance), 1.0f);

        try {
            graphics2D.setColor(color);

            graphics2D.fill(polygonFace1);
            graphics2D.fill(polygonFace2);
            graphics2D.fill(polygonFace3);

            graphics2D.setColor(Color.BLACK);

            graphics2D.draw(polygonFace1);
            graphics2D.draw(polygonFace2);
            graphics2D.draw(polygonFace3);
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }
    }

    private record CubeFace(int[][] value, double distance) {
    }

    private int[] calcPoint(int[] point) {
        Matrix posMatrix = new Matrix(new double[][]{
                {point[0]},
                {point[1]},
                {point[2]}
        });

        Matrix finalMatrix = factorMatrix.times(posMatrix.minus(camPosition));

        double dX = finalMatrix.get(0, 0);
        double dY = finalMatrix.get(1, 0);
        double dZ = finalMatrix.get(2, 0);

        return new int[]{
                (int) (((screenRelativeZ / dZ) * dX + screenRelativeX) * scaleFactor),
                (int) (((screenRelativeZ / dZ) * dY + screenRelativeY) * scaleFactor)
        };
    }
}
