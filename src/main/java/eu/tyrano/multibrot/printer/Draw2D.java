package eu.tyrano.multibrot.printer;

import eu.tyrano.multibrot.Gzip;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class Draw2D {

    public Draw2D(String[] args) {
        File directory = new File(args[1]);
        int width = Integer.parseInt(args[2]);
        int height = Integer.parseInt(args[3]);

        Gzip gzip = new Gzip();

        File[] filesUnOrdered = directory.listFiles((dir, name) ->
                name.matches("^mandelbrot_\\d+\\.csv.gzip$"));

        ArrayList<File> files = new ArrayList<>(List.of(filesUnOrdered != null ? filesUnOrdered : new File[0]));

        files.sort(Comparator.comparing((File o) ->
                Integer.valueOf(o.getName().split("\\.")[0].split("_")[1])));

        for (int i = 0; i < files.size(); i++) {

            File file = new File("mandelbrot_" + String.format("%06d", i) + ".csv.gzip");

            String mandelbrotValues = gzip.unzipString(file);

            BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            String[] values = mandelbrotValues.split(",");
            int x = 0;
            int y = 0;
            for (String value : values) {
                Color color = Color.getHSBColor(Float.parseFloat(value), 1.0f, 1.0f);

                bufferedImage.setRGB(x, y, color.getRGB());

                x++;
                if (x >= width) {
                    x = 0;
                    y++;
                }
            }

            File image = new File("mandelbrot_" + String.format("%06d", i) + ".png");

            try {
                ImageIO.write(bufferedImage, "PNG", image);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (i % (files.size() / 100d) == 0) {
                System.out.println(i / (files.size() / 100d) + "%");
            }
        }
    }
}
