package eu.tyrano.multibrot.printer;

public record CubePosition(short x, short y, short z) {
}