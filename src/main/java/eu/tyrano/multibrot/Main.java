package eu.tyrano.multibrot;

import eu.tyrano.multibrot.printer.Draw2D;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String version = getVersion();

        if (args.length == 7 && args[0].equals("--mandelbrot")) {
            new CalculateZoomMandelbrot(args);
        } else if (args.length == 6 && args[0].equals("--multibrot")) {
            new CalculateMultibrot(args);
        } else if (args.length == 7 && args[0].equals("--zoom")) {
            new CalculateSpecificZoomMandelbrot(args);
        } else if (args.length == 4 && args[0].equals("--draw2d")) {
            new Draw2D(args);
        } else if (args.length == 7 && args[0].equals("--mandelbulb")) {
            new CalculateMandelbulb(args).compute();
        } else if (args.length == 3 && args[0].equals("--gif")) {
            new GifMaker(args);
        } else if (args.length == 1 && args[0].equals("--version")) {
            System.out.println(version);
        } else {
            System.err.println("""
                    Bad arguments. Please follow one of these patterns :
                        --mandelbrot (max zoom) (power) (width) (height) (x) (y)
                        --multibrot (iteration) (power at start) (power at end) (width) (height)
                        --zoom (zoom) (power) (width) (height) (x) (y)
                        --mandelbulb (width) (height) (depth) (frames) (iteration) (scale factor)
                        --draw2d (path/to/directory) (width) (height)
                        --gif (path/to/directory) (delay)
                        --version""");
        }
    }

    private static String getVersion() {
        try (InputStream resource = Main.class.getClassLoader().getResourceAsStream("version.txt")) {
            if (resource == null) {
                return "unknown";
            }
            Scanner scanner = new Scanner(resource);

            StringBuilder builder = new StringBuilder();
            while (scanner.hasNext()) {
                builder.append(scanner.nextLine());
            }

            return builder.toString();

        } catch (NullPointerException | IOException e) {
            return "unknown";
        }
    }
}