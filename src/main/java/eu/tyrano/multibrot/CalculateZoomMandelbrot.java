package eu.tyrano.multibrot;

import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;

public class CalculateZoomMandelbrot {

    private final int maxZoom;
    private final double power;
    private final int width;
    private final int height;
    private final double x;
    private final double y;
    private final AtomicInteger increment;
    private final Gzip gzip;

    public CalculateZoomMandelbrot(String[] args) {
        maxZoom = Integer.parseInt(args[1]);
        power = Double.parseDouble(args[2]);
        width = Integer.parseInt(args[3]);
        height = Integer.parseInt(args[4]);
        x = Double.parseDouble(args[5]);
        y = Double.parseDouble(args[6]);

        gzip = new Gzip();

        increment = new AtomicInteger(-1);

        int coreCount = Runtime.getRuntime().availableProcessors() - 1;
        for (int i = 0; i < coreCount; i++) {
            new Thread(this::calcValues).start();
        }
    }

    private void calcValues() {
        while (increment.intValue() <= maxZoom) {
            int i = increment.addAndGet(1);
            String mandelbrotData = new Mandelbrot(
                    power, (int) -Math.log(i), width, height, Math.exp(i), x, y)
                    .calculate();

            File file = new File("mandelbrot_" + String.format("%06d", i) + ".csv.gzip");

            gzip.zipString(mandelbrotData, file);

            if ((increment.intValue() - 1) % (maxZoom / 100d) == 0) {
                System.out.println((increment.intValue() - 1) / (maxZoom / 100d) + "%");
            }
        }
        increment.addAndGet(1);
    }
}
