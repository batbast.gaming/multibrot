import org.junit.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.zip.GZIPOutputStream;

public class TestMandelbrot {

    @Test
    public void testFloatBinary() {
        int intBitsWrap = Float.floatToIntBits(113637.336722f);
        byte[] bytes = new byte[]{
                (byte) (intBitsWrap >> 24),
                (byte) (intBitsWrap >> 16),
                (byte) (intBitsWrap >> 8),
                (byte) (intBitsWrap)
        };

        System.out.println("bytes = " + Arrays.toString(bytes));

        int intBitsUnwrap =
                bytes[0] << 24 | (bytes[1] & 0xFF) << 16 | (bytes[2] & 0xFF) << 8 | (bytes[3] & 0xFF);

        System.out.println(Float.intBitsToFloat(intBitsUnwrap));
    }

    @Test
    public void testGzip() {
        try (GZIPOutputStream gos = new GZIPOutputStream(new FileOutputStream("test.gz"))) {
            while (true) {
                int intBits = Float.floatToIntBits(1234);
                gos.write(new byte[]{
                        (byte) (intBits >> 24),
                        (byte) (intBits >> 16),
                        (byte) (intBits >> 8),
                        (byte) (intBits)
                });
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}